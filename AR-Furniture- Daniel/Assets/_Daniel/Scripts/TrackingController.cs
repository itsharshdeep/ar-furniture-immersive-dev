﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Wikitude;
using UnityEngine.UI;

public class TrackingController : SampleController
{
	[Header ("Trackers")]
	[SerializeField] private InstantTracker horizontalTracker;
	[SerializeField] private InstantTracker vertitcalTracker;

	[Header ("Initialization Controls")]
	public GameObject InitializationControls;

	[SerializeField]Button productButton;
	public static TrackerType currentTrackingType;

	private InstantTrackingState _currentState = InstantTrackingState.Initializing;

	private bool _isTracking = false;

	[Header ("Floor Models")]
	public List<GameObject> ModelFloors;
	[Header ("Blind Models")]
	public List<GameObject> Modelblinds;

	private HashSet<GameObject> _activeModels = new HashSet<GameObject>();

	public Image ActivityIndicator;

	public Color EnabledColor = new Color(0.2f, 0.75f, 0.2f, 0.8f);
	public Color DisabledColor = new Color(1.0f, 0.2f, 0.2f, 0.8f);

	private MoveController _moveController;
	private ProductUIManager _productUiManager;

	private float _currentDeviceHeightAboveGround = 1.0f;
	public Text HeightLabel;

	private GridRenderer _gridRenderer;

	public HashSet<GameObject> ActiveModels {
		get { 
			return _activeModels;
		}
	}

	private void Awake() {
		Application.targetFrameRate = 60;
		_moveController = GetComponent<MoveController>();
		_productUiManager = GameObject.FindObjectOfType<ProductUIManager> ();
		_gridRenderer = GetComponent<GridRenderer>();
	}


	void Update ()
	{
		if (Input.GetKeyDown (KeyCode.Escape)) {
			if (_currentState == InstantTrackingState.Tracking) {
				//TODO : Need to modify as per the current mode Horizontal or vertical
				horizontalTracker.SetState (InstantTrackingState.Initializing);
				vertitcalTracker.SetState (InstantTrackingState.Initializing);
			} else {
				Application.Quit ();
			}
		}
	}

	//TODO : Need to modify as per the current mode Horizontal or vertical
	public void OnFIXButtonClicked ()
	{
		if (currentTrackingType == TrackerType.Horizontal) {
			horizontalTracker.SetState (InstantTrackingState.Tracking);
		} else {
			vertitcalTracker.SetState (InstantTrackingState.Tracking);
		}
	}


	public void OnBeginDragFloors (int modelIndex) {
		
		//if (_isTracking) {
			_productUiManager.Back ();
			currentTrackingType = TrackerType.Horizontal;
			// Create object
			GameObject modelPrefab = ModelFloors[modelIndex];
			Transform model = Instantiate(modelPrefab).transform;
			model.GetComponent<TrackerInfo> ().myTrackerInfo = currentTrackingType;
			_activeModels.Add(model.gameObject);
			// Set model position at touch position
			var cameraRay = Camera.main.ScreenPointToRay(Input.mousePosition);
			Plane p = new Plane(Vector3.up, Vector3.zero);
			float enter;
			if (p.Raycast(cameraRay, out enter)) {
				model.position = cameraRay.GetPoint(enter);
			//}

			// Set model orientation to face toward the camera
			Quaternion modelRotation = Quaternion.LookRotation(Vector3.ProjectOnPlane(-Camera.main.transform.forward, Vector3.up), Vector3.up);
			model.rotation = modelRotation;

			_moveController.SetMoveObject(model);
		}
	}
	public void OnBeginDragBlinds (int modelIndex) {
		//if (_isTracking) {
			_productUiManager.Back ();
			currentTrackingType = TrackerType.Vertical;
			// Create object
			GameObject modelPrefab = Modelblinds[modelIndex];
			Transform model = Instantiate(modelPrefab).transform;
		model.GetComponent<TrackerInfo> ().myTrackerInfo = currentTrackingType;
			_activeModels.Add(model.gameObject);
			// Set model position at touch position
			var cameraRay = Camera.main.ScreenPointToRay(Input.mousePosition);
			Plane p = new Plane(Vector3.up, Vector3.zero);
			float enter;
			if (p.Raycast(cameraRay, out enter)) {
				model.position = cameraRay.GetPoint(enter);
		//	}

			// Set model orientation to face toward the camera
			Quaternion modelRotation = Quaternion.LookRotation(Vector3.ProjectOnPlane(-Camera.main.transform.forward, Vector3.up), Vector3.up);
			model.rotation = modelRotation;

			_moveController.SetMoveObject(model);
		}
	}


	public void OnHeightValueChanged(float newHeightValue) {
		_currentDeviceHeightAboveGround = newHeightValue;
		HeightLabel.text = string.Format("{0:0.##} m", _currentDeviceHeightAboveGround);
		horizontalTracker.DeviceHeightAboveGround = _currentDeviceHeightAboveGround;
	}

	public void OnSceneRecognized(InstantTarget target) {
		SetSceneActive(true);
	}

	public void OnSceneLost(InstantTarget target) {
		SetSceneActive(false);
	}

	private void SetSceneActive(bool active) {
//		foreach (var button in Buttons) {
//			button.interactable = active;
//		}
		productButton.interactable = active;
		foreach (var model in _activeModels) {
			model.SetActive(active);
		}
		ActivityIndicator.color = active ? EnabledColor : DisabledColor;
		_gridRenderer.enabled = active;
		_isTracking = active;
	}


	public void OnStateChanged(InstantTrackingState newState) {
		_currentState = newState;
		if (newState == InstantTrackingState.Tracking) {
			InitializationControls.SetActive(false);
		} else {
			foreach (var model in _activeModels) {
				Destroy(model);
			}
			_activeModels.Clear();

			InitializationControls.SetActive(true);
		}
		_gridRenderer.enabled = true;
	}


}
