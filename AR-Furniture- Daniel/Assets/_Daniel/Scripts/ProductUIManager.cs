﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProductUIManager : MonoBehaviour
{

	[SerializeField]private GameObject productsPanel;
	[SerializeField]private GameObject floorProducts;
	[SerializeField]private GameObject blindProducts;

	void Awake ()
	{
		Back ();
	}

	public void OpenProducts ()
	{
		productsPanel.SetActive (true);
		OpenFloorProducts ();
	}

	public void OpenFloorProducts ()
	{
		floorProducts.SetActive (true);
		blindProducts.SetActive (false);
	}

	public void OpenBlindProducts ()
	{
		floorProducts.SetActive (false);
		blindProducts.SetActive (true);
	}

	public void Back ()
	{
		productsPanel.SetActive (false);
	}
}
